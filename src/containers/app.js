import React,{Component} from 'react'
import SearchBar from '../components/search-bar'
import VideoList from './video-list'
import VideoDetail from '../components/video-detail'
import axios from 'axios'
import Video from "../components/video";

const API_KEY = "api_key=5e60352cd41d3718b199b7709b91ccfb";
const API_END_POINT = "https://api.themoviedb.org/3/";
const SEARCH_URL = "search/movie?language=fr&include_adult=false";
const POPULAR_MOVIES_URL = "discover/movie?language=fr&sort_by=popularity.desc&include_adult=false&append_to_response=images";

class App extends Component{
    constructor(props){
        super(props);
        this.state = {movieList:{}, currentMovie:{}};
    }

    componentWillMount(){
        this.initMovies();
    }

    initMovies(){
        axios.get(`${API_END_POINT}${POPULAR_MOVIES_URL}&${API_KEY}`).then(function(response) {
            this.setState({movieList:response.data.results.slice(1,6), currentMovie:response.data.results[0]},function () {
                this.applyVideoToCurrentMovie();
            });
            /*console.log("------------");
            console.log('', this.state.movieList);
            console.log("------------");*/
        }.bind(this));
    }

    applyVideoToCurrentMovie(){
        axios.get(`${API_END_POINT}movie/${this.state.currentMovie.id}?${API_KEY}&append_to_response=videos&include_adult=false`).then(function(response) {
            const youtubeKey = response.data.videos.results[0].key;
            let newCurrentMovieState = this.state.currentMovie;
            newCurrentMovieState.videoId = youtubeKey;
            this.setState({currentMovie : newCurrentMovieState});
            /*console.log("------------");
            console.log('',newCurrentMovieState);
            console.log("------------");*/
        }.bind(this));
    }

    onclickListItem(movie){
        this.setState({currentMovie:movie},function () {
            this.applyVideoToCurrentMovie();
            this.setRecommandation();
        })
    }

    onClickSearch(searchText){
        if(searchText) {
            axios.get(`${API_END_POINT}${SEARCH_URL}&${API_KEY}&query=${searchText}`).then(function (response) {
                if(response.data && response.data.results[0]){
                    if(response.data.results[0].id !== this.state.currentMovie.id){
                        this.setState({currentMovie: response.data.results[0]},()=>{
                            this.applyVideoToCurrentMovie();
                            this.setRecommandation();
                        })
                    }
                }

                /*console.log("------------");
                console.log('', this.state.movieList);
                console.log("------------");*/
            }.bind(this));
        }
    }

    setRecommandation(){
        axios.get(`${API_END_POINT}movie/${this.state.currentMovie.id}/recommendations?${API_KEY}&language=fr`).then(function(response) {
            this.setState({movieList:response.data.results.slice(0,5)});
            /*console.log("------------");
            console.log('', this.state.movieList);
            console.log("------------");*/
        }.bind(this));
    }

    render() {
        const renderVideoList = () => {
            if(this.state.movieList.length>=5){
                return <VideoList movieList={this.state.movieList} callback={this.onclickListItem.bind(this)}/>
            }
        };
        return (
            <div>
                <div className="search_bar">
                    <SearchBar callback={this.onClickSearch.bind(this)}/>
                </div>

                <div className="row">
                    <div className="col-md-8">
                        <Video videoId={this.state.currentMovie.videoId}/>
                        <VideoDetail title={this.state.currentMovie.title} description={this.state.currentMovie.overview}/>
                    </div>
                    <div className="col-md-4">
                        {renderVideoList()}
                    </div>
                </div>

            </div>
        )
    }
}

export default App;
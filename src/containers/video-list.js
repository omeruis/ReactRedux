import React from 'react'
import VideoListItem from '../components/video-list-item'

const VideoList = (props) => {
    const {movieList} = props;
    /*console.log("*********");
    console.log('',movieList);
    console.log("*********");*/
    return(
        <div>
            <ul>
                {
                    movieList.map(movie =>{
                        return <VideoListItem key={movie.id} movie={movie} callback={receiveCallback}/>
                    })
                }
            </ul>
        </div>
    );
    function receiveCallback(movie) {
        props.callback(movie);
    }
};

export default VideoList;